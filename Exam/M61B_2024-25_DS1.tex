\documentclass[a4paper,12pt,reqno]{amsart}
\usepackage{lille}
\lilleset{
  solutions,
  % sans enonces,
  titre=\sisujet{Devoir surveillé}\sisolutions{Solutions du devoir surveillé},
  date=1 mars 2025,
  duree=2 heures,
}
\input{m61proba}

\begin{document}
\sisujet{
  \tsvp
  \vspace{7mm}
  \attention~
  \emph{Les documents et les objets électroniques sont interdits. Les exercices sont indépendants. Toutes les réponses doivent être justifiées.}
  \vspace{14mm}
}


% -----------------------------------------------
\begin{exo}

  On considère $\R$ muni de la tribu borélienne $\mathcal{B}(\R)$ et de la mesure de Lebesgue $\lambda$.\\
  Soit $\suite[n\geq 1]{a_n}$ une suite de nombres réels. On pose
  \[
    A\coloneqq\ensemble{x\in\R}{ \exists n\in\N^*, |x-a_n|\leq 2^{-n}}.
  \]
  \begin{enumerate}
    \item Justifier que $A\in\mathcal{B}(\R)$. \indication{considérer les $A_n\coloneqq\ensemble{x\in\R}{|x-a_n|\leq 2^{-n}}$.}
    \item Montrer que $1\leq \lambda(A)\leq 2$.
    \item Calculer $\lambda(A^c)$ où $A^c$ désigne le complémentaire de $A$ dans $\R$.
    \item Calculer $\lambda(A)$ lorsque $a_n=0$ pour tout $n\geq 1$.
    \item Calculer $\lambda(A)$ lorsque $a_n=n$ pour tout $n\geq 1$.
    \item Un borélien de $\R$ ayant une mesure de Lebesgue finie, est-il nécessairement borné?
  \end{enumerate}
\end{exo}

\begin{solution}
  \begin{enumerate}
    \item Les $A_n\coloneqq\ensemble{x\in\R}{|x-a_n|\leq 2^{-n}} = [a_{n}-2^{-n}, a_{n}+2^{-n}]$ sont des intervalles fermés, donc sont des boréliens. Ainsi $A = \bigcup_{n\in\N^*} A_n$ est une union dénombrable de boréliens, donc, c'est un borélien.
    \item Nous avons $\lambda(A_n) = 2^{-(n-1)}$. De plus, on a $\lambda(A_1) \leq \lambda(A) \leq \sum_{n=1}^{+\infty} \lambda(A_n)$. Et comme $\lambda(A_1) = 1$ et $\sum_{n=1}^{+\infty} \lambda(A_n) = 1+\frac{1}{2}+\frac{1}{4}+\cdots = 2$, on a $1 \leq \lambda(A) \leq 2$.
    \item Comme $\lambda(A) + \lambda(A^c) = \lambda(\R) = +\infty$, on a $\lambda(A^c) = +\infty - \lambda(A) = +\infty$, puisque $\lambda(A) \leq 2$. Donc $\lambda(A^c) = +\infty$.
    \item Soient $a_n=0$ pour tout $n\geq 1$. Alors $A_n = [-2^{-n}, 2^{-n}] \supset [-2^{-(n+1)}, 2^{-(n+1)}] = A_{n+1}$, donc $A = \bigcup_{n\in\N^*} A_n = A_1 = [-\frac{1}{2}, \frac{1}{2}]$. Ainsi $\lambda(A) = 1$.
    \emph{Par conséquent, la borne inférieure de $\lambda(A)$ est optimale.}
    \item Soient $a_n=n$ pour tout $n\geq 1$, avec $A_n = [n-2^{-n}, n+2^{-n}]$. Et comme, pour $n<m$, on a $n+2^{-n} < m - 2^{-m}$, il en résulte que les $A_n$ sont tous disjoints. Ainsi $\lambda(A) = \sum_{n\in\N^*} \lambda(A_n) = \sum_{n\in\N^*} 2^{-n} = 2$. \emph{Par conséquent, la borne supérieure de $\lambda(A)$ est optimale.}
    \item Dans le cas de la question précédente $A$ est un borélien non borné, car contenant $\N^*$, de mesure finie $2$. Donc, un borélien de $\R$ ayant une mesure de Lebesgue finie n'est pas nécessairement borné.
  \end{enumerate}
\end{solution}

% -----------------------------------------------
\begin{exo}
  \begin{enumerate}
    \item Soit $X$ une variable aléatoire suivant une loi de Poisson de paramètre $\lambda$. Déterminer son espérance $\EE{X}$ à l'aide d'un calcul explicite, vu en cours.
    \item Soient $X_1$ et $X_2$ deux variables aléatoires indépendantes de lois de Poisson de paramètres $\lambda_1$ et $\lambda_2$ respectivement.
    \begin{enumerate}
      \item Soit $n\in \N$, déterminer $\PP{X_1+X_2=n}$.
      \item En déduire l'espérance $\EE{X_1+X_2}$.
    \end{enumerate}
  \end{enumerate}
\end{exo}

\begin{solution}
  \begin{enumerate}
    \item\label{moypoiss} Comme $X\sim\Poi{\lambda}$, nous avons $\PP{X=k} = \frac{\lambda^k}{k!} e^{-\lambda}$. Ainsi
    \begin{gather*}
      \EE{X}
      = \sum_{k=0}^{+\infty} k \PP{X=k}
      = \sum_{k=1}^{+\infty} k \frac{\lambda^k}{k!} e^{-\lambda}
      = e^{-\lambda} \sum_{k=1}^{+\infty} \frac{\lambda^k}{(k-1)!}
      = e^{-\lambda} \lambda \sum_{k=0}^{+\infty} \frac{\lambda^k}{k!}
      = e^{-\lambda} \lambda e^{\lambda}
      = \lambda.
    \end{gather*}
    \item
    \begin{enumerate}
      \item Nous avons, pour $n\in \N$, en utilisant l'indépendance de $X_{1}$ et $X_{2}$,
      \begin{align*}
        \PP{X_1+X_2=n}
          &= \sum_{k=0}^{n} \PP{X_1=k, X_2=n-k}
          = \sum_{k=0}^{n} \PP{X_1=k} \PP{X_2=n-k}\\
          &= \sum_{k=0}^{n} \frac{\lambda_1^k}{k!} e^{-\lambda_1} \frac{\lambda_2^{n-k}}{(n-k)!} e^{-\lambda_2}
          = e^{-(\lambda_1+\lambda_2)}\frac{1}{n!} \sum_{k=0}^{n} \frac{n!}{k!(n-k)!} \lambda_1^k \lambda_2^{n-k}\\
          &= e^{-(\lambda_1+\lambda_2)}\frac{1}{n!} \sum_{k=0}^{n} \binom{n}{k} \lambda_1^k \lambda_2^{n-k}
          = e^{-(\lambda_1+\lambda_2)} \frac{(\lambda_1+\lambda_2)^n}{n!}.
      \end{align*}
      Ainsi, on a $X_1+X_2 \sim \Poi{\lambda_1+\lambda_2}$.
      \item Pour trouver $\EE{X_1+X_2}$, nous pouvons utiliser les questions précédentes pour affirmer que $\EE{X_1+X_2} = \lambda_1+\lambda_2$. Mais nous pouvons aussi nous passer de la question précédente en utilisant la linéarité de l'espérance, puis la question \eqref{moypoiss}, pour obtenir $\EE{X_1+X_2} = \EE{X_1} + \EE{X_2} = \lambda_1 + \lambda_2$.
    \end{enumerate}
  \end{enumerate}
\end{solution}

% -----------------------------------------------
\begin{exo}

  Pour une certaine expérience aléatoire, l'ensemble des résultats possibles est
  \[
    \Omega = \{2,3,4,5,\ldots\} = \N-\{0,1\}
  \]
  et l'ensemble des événements observables est $\mathcal{F}=\mathcal{P}(\Omega)$.
  \begin{enumerate}
    \item À quelles conditions sur la famille de réels $\suite[i \geq 2]{p_i}$ définit-on une probabilité sur $(\Omega,\mathcal{F})$ en posant $\PP{\{i\}} = p_i$ pour chaque $i$ de $\Omega$?
    \item Vérifier que ces conditions sont réalisées pour les
    \[
      p_i = (i-1) \frac{ 2^{i-2} }{3^i}, \quad i \geq 2.
    \]
    \indication{on pourra utiliser directement la formule du cours donnant l'espérance d'une variable aléatoire suivant une loi géométrique, à condition de préciser clairement son application dans ce cas.}
  \end{enumerate}
    On tire un nombre $ N $ au hasard en utilisant cette expérience. On a alors
    \[
      \PP{N=i} = (i-1) \frac{ 2^{i-2} }{3^i}, \quad\forall  i \geq 2.
    \]
    Une fois $N$ déterminé, on choisit au hasard (avec équiprobabilité) un entier $X$ tel que \text{$0<X<N$}.
  \begin{enumerate}[resume]
    \item Déterminer la valeur de $\PP{X=k\;|\;N=i}$ pour chaque $k \ge 1$ et chaque $i \ge 2$.
    \item En déduire $\PP{X=k}$. Quelle est la loi de $X$ ?
    \item Calculer les $\PP{N-X=j\;|\;N=i}$ et trouver, sans calcul, la loi de la variable aléatoire $N-X$.
    \item Les variables $X$ et $N-X$ sont-elles indépendantes ?
  \end{enumerate}

\end{exo}

\begin{solution}
  \begin{enumerate}
    \item Une probabilité sur un ensemble dénombrable de valeurs est donnée par une suite de nombres positifs de somme $1$. Pour $\Omega = \{2,3,4,5,\ldots\}$, la famille des réels $\suite[i \ge 2]{p_i}$ définit une probabilité si les $p_i$ sont tous positifs et si $\sum_{i=2}^{+\infty} p_i = 1$.

    \item Il est clair que, pour tout $i \ge 2$, on a $p_i = (i-1) \frac{ 2^{i-2} }{3^i} \ge 0$. D'autre part, un rapide changement de variable donne
    \[
       \sum_{i=2}^{+\infty} p_i = \sum_{i=2}^{+\infty} (i-1) \frac{ 2^{i-2} }{3^i} = \sum_{j=1}^{+\infty} j \frac{ 2^{j-1} }{3^{j+1}}.
    \]
    On utilise ici l'espérance d'une loi géométrique de paramètre $1/3$, qui vaut $3$:
    \[
      \sum_{i=2}^{+\infty} p_i = \frac13 \sum_{j=1}^{+\infty} j \frac13 \pa{\frac23}^{j-1} = \frac13 \times 3 = 1.
    \]
    \item Une fois que $N$ est déterminé, on choisit au hasard avec équiprobabilité un entier $X$ tel que $0<X<N$. Par conséquent $\PP{X=k\;|\;N=i}= \frac{1}{i-1}$ car il y a $i-1$ valeurs possibles strictement comprises entre $0$ et $i$. Ceci est valable pour tous les $k$ tels que $0<k<i$. Quand $k\ge i$ on a $\PP{X=k\;|\;N=i}= 0$.
    \item On fixe $k$ dans $\N^*$ et on calcule $\PP{X=k}$ grâce à la formule de conditionnement par tous les cas possibles:
    \begin{align*}
      \PP{X=k} & = \sum_{i=2}^{+\infty} \PP{X=k\;|\;N=i} \PP{N=i} \\
             & \text{ la probabilité conditionnelle est nulle sauf si $i>k$ i.e. $i \ge k+1$} \\
             & = \sum_{i=k+1}^{+\infty} \PP{X=k\;|\;N=i} \PP{N=i} \\
             & = \sum_{i=k+1}^{+\infty} \frac{1}{i-1} \times (i-1) \frac{ 2^{i-2} }{3^i} \\
             & = \frac14 \sum_{i=k+1}^{+\infty} \pa{\frac23}^i
              = \frac14 ~\frac{ \pa{\frac23}^{k+1} }{1-\frac23}
              = \frac34 \pa{\frac23}^{k+1}
              = \frac13 \pa{\frac23}^{k-1}
    \end{align*}
    $X$ suit la loi géométrique de paramètre $1/3$.
    \item Quand on sait que $N=i$ dire que $N-X=j$  revient à dire que $X=i-j$:
    \[
       \PP{N-X=j\;|\;N=i}
        = \PP{X=i-j\;|\;N=i}
        = \begin{cases}
            \frac{1}{i-1} & \text{ si } i>j>0, \\
            0             & \text{ sinon}.
          \end{cases}
    \]
    On constate que les $\PP{N-X=j\;|\;N=i}$ sont égales aux $\PP{X=j\;|\;N=i}$. Le calcul de la loi de $N-X$ sera donc identique au calcul de la loi de $X$, et donnera le même résultat: $N-X$ suit la loi géométrique de paramètre $1/3$.
    \item Pour tout $k$ et tout $j$ de $\N^*$
    \begin{align*}
      \PP{X=k,N-X=j}
        & = \PP{X=k, N=j+k} \\
        & = \PP{X=k\;|\;N=k+j} \PP{N=k+j} \\
        & = \frac{1}{k+j-1} \times (k+j-1) \frac{ 2^{k+j-2} }{3^{k+j}} \\
        & = \frac{ 2^{k+j-2} }{3^{k+j}},
    \end{align*}
    et puisque $X$ et $N-X$ suivent la loi géométrique de paramètre $1/3$,
    \[
      \PP{X=k}\PP{N-X=j} = \frac13 \pa{\frac23}^{k-1} \times \frac13 \pa{\frac23}^{j-1} = \frac{ 2^{k+j-2} }{3^{k+j}}.
    \]
    Ainsi $\PP{X=k,N-X=j} = \PP{X=k}\PP{N-X=j}$ et par conséquent $X$ et $N-X$ sont indépendantes.
  \end{enumerate}
\end{solution}


\end{document}

