# [<img src="https://about.gitlab.com/images/press/logo/svg/gitlab-logo-500.svg" height="50" style="vertical-align: middle">](https://gitlab.univ-lille.fr/tzanev/l3m61proba/) [m61proba](https://gitlabpages.univ-lille.fr/tzanev/l3m61proba)

Les feuilles de td du module M61B - « Probabilités-Intégration » de la L3 Mathématiques à l'Université de Lille.

## 2024/25

Dans [ce dépôt](https://gitlab.univ-lille.fr/tzanev/l3m61proba/) vous pouvez trouver les sources LaTeX et les PDFs des documents suivants _(compilés avec [tectonic](https://tectonic-typesetting.github.io))_ :

### Les feuilles de td
- TD n°1 **[[pdf](TDs/M61B_2024-25_TD1.pdf)]** [[tex](TDs/M61B_2024-25_TD1.tex)]
- TD n°2 **[[pdf](TDs/M61B_2024-25_TD2.pdf)]** [[tex](TDs/M61B_2024-25_TD2.tex)]
- TD n°3 **[[pdf](TDs/M61B_2024-25_TD3.pdf)]** [[tex](TDs/M61B_2024-25_TD3.tex)]
- TD n°4 **[[pdf](TDs/M61B_2024-25_TD4.pdf)]** [[tex](TDs/M61B_2024-25_TD4.tex)]
- TD n°5 **[[pdf](TDs/M61B_2024-25_TD5.pdf)]** [[tex](TDs/M61B_2024-25_TD5.tex)]
- TD n°6 **[[pdf](TDs/M61B_2024-25_TD6.pdf)]** [[tex](TDs/M61B_2024-25_TD6.tex)]
- TD n°7 **[[pdf](TDs/M61B_2024-25_TD7.pdf)]** [[tex](TDs/M61B_2024-25_TD7.tex)]
- TD n°8 **[[pdf](TDs/M61B_2024-25_TD8.pdf)]** [[tex](TDs/M61B_2024-25_TD8.tex)]

### Les sujets d'examens et leurs solutions

- DS1 **[[sujet](Exam/M61B_2024-25_DS1_sujet.pdf)]** **[[solutions](Exam/M61B_2024-25_DS1_solutions.pdf)]** [[tex](Exam/M61B_2024-25_DS1.tex)]
 
### Source complémentaires

Pour compiler ces fichiers vous avez besoin des styles et des logos suivants :
- [lille.sty](TDs/lille.sty)
- [m61proba.tex](TDs/m61proba.tex)
- [lille.pdf](TDs/images/lille.pdf)
- [tsvp.pdf](Exam/images/tsvp.pdf)
- [attention.pdf](Exam/images/attention.pdf)

### Récupération

Vous pouvez obtenir l'intégralité de [ce dépôt](https://gitlab.univ-lille.fr/tzanev/l3m61proba/) de deux façons faciles :

- en téléchargeant le [zip](https://gitlab.univ-lille.fr/tzanev/l3m61proba/-/archive/main/m61proba-main.zip) qui contient la dernière version des fichiers,
- en clonant le dépôt entier, l'historique y compris, en utilisant la commande `git` suivante

  ```shell
  git clone https://gitlab.univ-lille.fr/tzanev/l3m61proba.git
  ```

## Historique

### 2023/24

Vous pouvez trouver les sources LaTeX et les PDFs de 2024, ainsi que les annales des années précédentes, à l'adresse suivante :

https://gitlabpages.univ-lille.fr/tzanev/l3m61proba/2024/

## Licence

[MIT](LICENSE)
