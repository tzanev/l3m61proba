\documentclass[a4paper,11pt,reqno]{amsart}
\usepackage{lille}
\input{m61proba}

\lilleset{titre=TD4 - Variables aléatoires discrètes}

\begin{document}

\emph{Dans toute la fiche, on supposera que les variables aléatoires d'un même exercice sont définies sur un même espace de probabilités $(\Omega, \mathscr{F}, \P)$.}


% ===============================================
\section{Pour s'échauffer : événements, calculs de probabilités}
% ===============================================

% -----------------------------------------------
\begin{exo}\emph{(Les chats de Shrödinger)}

  Considérons l'expérience de pensée suivante : on place 20 chats dans des boites individuelles. Par ailleurs, en plus des chats,
  \begin{itemize}
    \item 10 boites ne contiennent rien d'autre,
    \item 5 boites contiennent 3g d'arsenic,
    \item 5 boites contiennent 10g d'arsenic.
  \end{itemize}
  Au bout d'une heure, la probabilité qu'un chat soit encore en vie dans la boite est égale à 1 si la boite ne contient pas d'arsenic, 0,6 si la boite contient 3g d'arsenic, et 0,2 si la boite contient 10g d'arsenic\footnote{Aucun chat n'a été maltraité pour l'élaboration de cette expérience de pensée !}. On choisit une des 20 boites au hasard.
  \begin{enumerate}
    \item Quelle est la probabilité que le chat qui s'y trouve soit encore en vie au bout d'une heure ?
    \item Le chat est en vie. Quelle est la probabilité que la boite ait été vide quand on l'y a placé ?
  \end{enumerate}

\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Contrôleur contre fraudeur)}

  Une compagnie de métro applique les tarifs suivants : un ticket coûte $1$\,€, une amende est de $20$\,€ pour la première infraction, $40$\,€ pour la deuxième, et $400$\,€ pour la troisième. La probabilité $p$ d’être contrôlé par trajet ($0 < p < 1$) est connue uniquement de la compagnie. Un fraudeur voyage sans payer jusqu’à sa deuxième amende, après quoi il cesse. On note $T$ le nombre total de trajets effectués jusqu’à la deuxième amende, et $q = 1 - p$ la probabilité d’un trajet sans contrôle.
  \begin{enumerate}
    \item Montrer que la loi de $T$ est donnée par
    \[
      \PP{T=k}=(k-1)p^2q^{k-2},\quad k\geq 2.
    \]
    \item Pour $n\in\N^\star$, calculer $\PP{T>n}$.

    \emph{Indication :} On pourra commencer par chercher une formule explicite pour la somme de la série entière $f(x) \coloneqq \sum_{k=n+1}^{+\infty}x^{k-1}$, puis pour sa dérivée terme à terme.

    \item Calculer numériquement $\PP{T>60}$ (pourquoi s'intéresse-t-on à cette quantité~?) lorsque $p=1/10$ et lorsque $p=1/20$.
  \end{enumerate}
\end{exo}


\section{Une variable aléatoire discrète}

%-----------------------------------
\begin{exo}\emph{(Loi image)}

  Soit $X$ une variable aléatoire qui suit une loi binomiale $\mathcal{B}(3,\frac{1}{4})$ et $f$ la fonction \emph{parité}, c.-à-d. $f(n) = 1$ si $n$ est impaire et $f(n) = 0$ si $n$ est paire.
  \begin{enumerate}
    \item Pourquoi $f(X)$ est une variable aléatoire (discrète) ?
    \item Déterminer la loi de $f(X)$.
  \end{enumerate}
\end{exo}

%-----------------------------------
\begin{exo}\emph{(Deux dés)}

  Nous lançons deux dés non truqués.
  \begin{enumerate}
    \item La variable aléatoire $X$ est la somme des points obtenus. Quelle est la distribution de $X$ ?
    \item Même question pour la variable aléatoire $Y$ égale au minimum des deux résultats obtenus.
    \item On considère la variable aléatoire $Z = (X-7)^{2}$. Quelle est la distribution de $Z$ ?
  \end{enumerate}
\end{exo}

% -----------------------------------
\begin{exo}\emph{(Contrôle de qualité)}

  Une machine-outil produit à la chaîne des objets manufacturés et l'on sait qu'en période de marche normale la probabilité pour qu'un objet soit défectueux est $p$. On se propose de vérifier la machine. À cet effet, on définit la variable aléatoire $T_r$ égale au nombre minimum de prélèvements successifs qu'il faut effectuer pour amener $r$ objets défectueux. Calculer la loi de $T_r$.
\end{exo}

% -----------------------------------------------
\begin{exo}[.7]\emph{(Maximum de la loi de Poisson)}

  Soit $X$ une variable aléatoire réelle suivant une loi de Poisson $\mathcal{P}(\lambda)$. Pour quelle(s) valeur(s) de $k\in\mathbb{N}$ la probabilité $P(X=k)$ est maximale ?
\end{exo}


% -----------------------------------------------
\begin{exo}\emph{(Une autre formule pour l'espérance)}

  Soit $X$ une variable aléatoire d'espace d'états $\{0,\dots, N\}$. Montrer que
  \[
    \EE{X}=\sum_{k=0}^{N-1}\PP{X>k}.
  \]
  Que peut-on dire si $X$ prend ses valeurs dans $\N$ tout entier ?
\end{exo}


\section{Plusieurs variables aléatoires discrètes}

% -----------------------------------------------
\begin{exo}\emph{(Sur la loi uniforme)}

  Soit $X$, $Y$ deux variables aléatoires indépendantes suivant la loi uniforme sur $\{1,\ldots,n\}$.
  \begin{enumerate}
    \item Déterminer $\PP{X=Y}$.
    \item Déterminer $\PP{X\geq Y}$.
    \item Déterminer la loi de $X+Y$.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Loi jointe)}

  On considère deux variables $X$ et $Y$ à valeurs dans $\N$. On suppose que l'on a, pour tout $(j,k)\in \N^2$
  \[
    \PP{X=j, Y=k}=\alpha\frac{(j+k)(1/2)^{j+k}}{j!k!},
  \]
  la \emph{loi jointe} du couple de variables aléatoires $(X,Y)$, dont $X$et $Y$ sont les \emph{marginales}.
  \begin{enumerate}
    \item Déterminer le réel $\alpha$.
    \item À partir de cette formule, déterminer la loi de $X$ et la loi de $Y$.
    \item Les variables $X$ et $Y$ sont-elles indépendantes ?
    \item Montrer que $ \EE{2^{X+Y}}<\infty$, et la calculer.
  \end{enumerate}
\end{exo}

% -----------------------------------
\begin{exo}\emph{(Mélange de lois, DS1 2024)}

  Soient deux réels $\mu>0$ et $p\in]0,1[$. Dans une banque, le nombre de chèques émis par les clients en un jour est une variable aléatoire $X$ qui suit la loi de Poisson $\mathcal{P}(\mu)$. Pour chaque chèque émis, la probabilité que ce chèque soit sans provision est $p$.

  On appelle $Y$ le nombre de chèques émis sans provision lors d'une journée.
  \begin{enumerate}
    \item Soit $n \in \N$. Trouver $\PP{Y=k \mid X=n}$ et $\PP{X-Y=l \mid X=n}$ suivant les valeurs de $k\in\N$ et $l\in\N$.
    \item Utiliser la formule des probabilités totales pour déterminer la loi de $Y$ et celle de $X-Y$.
    \item Calculer $\PP{X-Y=l \mid Y=k}$ en fonction de $k$ et $l$. Les variables $X-Y$ et $Y$ sont-elles indépendantes ?
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Minimum de lois géométriques)}

  Soit $N\in \N^*$ et $p\in ]0,1[$. On considère $N$ variables indépendantes $X_1,\dots, X_N$, chacune de loi géométrique de paramètre $p$.
  \begin{enumerate}
    \item Soit $i\in\{1,\dots, N\}$ et $n\in \N^*$, déterminer $\PP{X_i\leq n}$ puis $\PP{X_i>n}$.
    \item On définit la v.a. $Y$ par $Y=\min_{1\leq i\leq N}X_i$, c.-à-d. que pour tout $\omega\in \Omega$, $Y(\omega)=\min\{X_i(\omega), \dots ,X_N(\omega)\}$.
    \begin{enumerate}
      \item Soit $n\in\N^*$, calculer $\PP{Y>n}$. En déduire $ \PP{Y\leq n}$. Quelle est la loi de $Y$ ?
      \item $Y$ admet-elle une espérance finie ? Si oui, la calculer.
    \end{enumerate}
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Deux propriétés classiques des lois de Poisson)}

  \begin{enumerate}
    \item Soient $X_1$ et $X_2$ deux v.a. indépendantes de lois de Poisson de paramètres $\lambda_1$ et $\lambda_2$.
    \begin{enumerate}
      \item Soit $n\in \N$, déterminer $\PP{X_1+X_2=n}$.
      \item En déduire l'espérance $\EE{X_1+X_2}$ et la variance $\VV{X_1+X_2}$.
    \end{enumerate}
    \item On considère deux variables aléatoires $X$ et $Y$. On suppose que $Y$ suit une loi de Poisson de paramètre $\lambda$, et qu'il existe $p\in [0,1]$ tel  que pour tout $ k\leq m$,
    \[
      \PP{X=k\mid Y=m}=\binom{m}{k}p^k(1-p)^{m-k}.
    \]
  \end{enumerate}

  \noindent On dira alors que la distribution \emph{conditionnelle} de $X$ sachant $\{Y=m\}$ est Binomiale$(m,p)$. Déterminer la loi de $X$.
\end{exo}


% ===============================================
\section{Borel-Cantelli}
% ===============================================

% -----------------------------------------------
\begin{exo}\emph{(Retours à l'origine)}

  Considérons un ivrogne qui, à chaque étape, fait un pas en avant avec une probabilité $p > \frac{1}{2}$ et un pas en arrière avec une probabilité $q=1-p$. Soit $\suite[n\geq1]{X_n}$ une suite de variables aléatoires indépendantes de même loi décrivant les déplacements de l'ivrogne, où
  \[
    P(X_i=+1)=p, \qquad P(X_i=-1)=q.
  \]
  On pose la variable \enquote{position} $ S_n \coloneqq \sum_{i=1}^{n} X_i $ et l'événement \enquote{retour à l'origine} $ A_n \coloneqq \{S_n=0\}$.
  Déterminer la probabilité que l'ivrogne retourne une infinité de fois à l'origine.\\
  \emph{Rappelons la formule de Stierling $n! \sim_\infty \sqrt{2\pi n} \left(\frac{n}{e}\right)^n $.}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Désaccord avec Borel ?)}

  Considérons un jeu infini de pile ou face avec une pièce équilibrée et définissons la suite d'événements
  \[
    F_{n} : \text{\enquote{le premier et le $n$-ème jet donnent face}}.
  \]
  pour $n\in\N^*$ et $F=\limsup F_{n}$ l'événement \enquote{les événements $F_n$ se produisent infiniment souvent}.
  \begin{enumerate}
    \item Montrer que
    \[
      \sum_{n\in\N^*}P(F_n)=+\infty\quad \textrm{et}\quad P(F)=\frac{1}{2}.
    \]
    \item Expliquer pourquoi ceci n'est en contradiction ni avec le lemme de Borel-Cantelli, ni avec la loi du \enquote{zéro-un} de Borel\footnote{Appelé également \enquote{deuxième lemme de Borel-Cantelli}.}.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}[.7]\emph{(Le paradoxe du singe savant)}

  Un singe (immortel) tape indéfiniment et au hasard sur le clavier d'une machine à écrire. Montrer que presque sûrement le singe écrira un nombre infini de fois l'énoncé de ce même exercice.
\end{exo}


% -----------------------------------------------
\begin{exo}

  Une pièce de monnaie équilibrée est lancée une infinité de fois. Nous cherchons à déterminer la probabilité d'obtenir une infinité de fois un million de \enquote{pile}s consécutifs.

  Les lancers sont modélisés par une suite $ (X_n)_{n \in \mathbb{N}} $ de variables aléatoires indépendantes de loi de Bernoulli de paramètre $1/2$, où $X_n = 1$ correspond à \enquote{pile} et $X_n = 0$ à \enquote{face}. Les événements considérés sont alors de la forme
  \[
    A_n = \{X_n = X_{n+1} = \cdots = X_{n+999999} = 1\}, \quad n \in \mathbb{N}.
  \]

  \begin{enumerate}
    \item Les événements $A_n$ sont-ils indépendants ?
    \item Considérer la sous-suite $B_n = A_{10^6 n}$, $n \in \mathbb{N}$.
    \item Conclure.
  \end{enumerate}
\end{exo}

\end{document}
