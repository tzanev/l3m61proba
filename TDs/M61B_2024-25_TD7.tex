\documentclass[a4paper,11pt,reqno]{amsart}
\usepackage{lille}
\input{m61proba}

\lilleset{titre=TD7 - Lois classiques continues et moments}

\begin{document}

% ===============================================
\section{La loi uniforme}
% ===============================================

% -----------------------------------------------
\begin{exo}\emph{(Loi uniforme)}

  Une variable aléatoire réelle continue $X$ prenant des valeurs dans $[a,b]$ est appelée \emph{uniforme}, et on note $X\sim\Uni{[a,b]}$, si sa fonction densité est :
  \[
    \rho_X = \frac{1}{b-a}\1_{[a,b]}.
  \]
  \begin{enumerate}
    \item Calculez la moyenne $\mathbb{E}(X)$, la variance $\VV{X}$ et l'écart type $\sigma(X)$ de $X$.
    \item Pour quelles $a$ et $b$, la variable $X$ est centrée réduite ?
    \item Calculez les moments $\EE{X^{n}}$ de $X$ pour $n\in\N$.
    \item Si $U$ est uniforme sur $[0,1]$, quelle est la distribution de $aU+b$ pour $a,b \in \mathbb{R}$ ? En déduire la distribution de $1-U$.
    \item \emph{Application.} Le temps d'attente (en minutes) pour accéder à des données suit une loi uniforme $\mathcal{U}([1,7])$. Déterminer l'espérance du temps d'attente et son écart type.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Fonctions de la loi uniforme)}

  \begin{enumerate}
    \item Soit $U\sim\Uni{[0,1]}$ une variable aléatoire réelle de loi uniforme et $X = P(U) = a_{n}U^{n}+\dots + a_{1}U + a_{0}$. Calculer $\EE{X}$ et $\VV{X}$ en fonction des $\suite[i=0,\dots,n]{a_{i}}$.
    \item Soit $Y\sim\Uni{[0,\pi]}$, une variable aléatoire réelle de loi uniforme. Calculer $\EE{\sin(Y)}$ et $\VV{\sin(Y)}$, ainsi que $\EE{\cos(Y)}$ et $\VV{\cos(Y)}$.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Simulation par la loi uniforme)}

  Soit $F:\R\to\R$ la fonction de répartition d'une variable aléatoire réelle $X$. Pour $u\in]0,1[$, on pose
  \[
    G(u)=\inf\ensemble{x\in\R}{F(x)\geq u}.
  \]
  On appelle $G$ \emph{l'inverse généralisée} de $F$.
  \begin{enumerate}
    \item Montrer que $\ensemble{x\in\R}{F(x)\geq u}$ est un intervalle fermé minoré et non majoré. En déduire que $G$ est bien définie et que $\ensemble{x\in\R}{F(x)\geq u} = [G(u),+\infty[$.
    \item Démontrer que, pour tout $x\in\R$ et tout $u\in]0,1[$,
    \[
      F(x)\geq u\; \Longleftrightarrow \; x\geq G(u).
    \]
    \item Pour $U\sim\Uni{]0,1[}$, quelle est la fonction de répartition de $G(U)$ ? Conclure.
    \item Trouver $G$ telle que $G(U)$ soit discrète uniforme sur $\{n,n+1,\dots,m\}$ quand $U\sim\Uni{]0,1[}$.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Ni continue, ni discrète)}

  Pour $X$ variable positive de fonction de répartition $F_{X}$, démontrer que
    \[
      \EE{X}=\int_0^{+\infty} 1-F_X(t)dt \quad\in \R\cup\{\infty\}.
    \]
  \indication{Utiliser l'exercice précédent pour écrire $\EE{X}=\int_0^{1} G(t)dt$.}

  Soit $X$ une variable uniforme sur $[1,3]$ et $a\in[1,3]$.
  \begin{enumerate}
    \item Quelle est la loi de la variable aléatoire $Y=\min\{X,a\}$ ?
    \item Admet-elle une espérance ? Si oui, la calculer.
    \item Que vaut cette espérance si $a=1$ ? $a=3$ ? Est-ce que vous auriez pu trouver ces deux résultats autrement ?
  \end{enumerate}
\end{exo}



% ===============================================
\section{D'autres lois classiques}
% ===============================================

% -----------------------------------------------
\begin{exo}\emph{(Loi exponentielle)}

  Une variable aléatoire réelle positive $X$ est appelée \emph{exponentielle} avec le paramètre $\lambda > 0$ si $X$ admet pour fonction densité :
  \[
    \rho_X(t) = \lambda e^{-\lambda t} \text{ pour } t \in \mathbb{R}_{+}.
  \]
  \begin{enumerate}
    \item Calculez la moyenne $\mathbb{E}(X)$, la variance $\VV{X}$ et l'écart type $\sigma(X)$ de $X$.
    \item Déterminer une fonction $G$ telle que pour $U\sim\Uni{[0,1]}$, la variable $G(U)$ soit exponentielle.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Loi normale)}

  Soit $X$ une variable aléatoire normale centrée réduite.
  \begin{enumerate}
    \item Justifier que $X$ admet des moments à toute ordre.
    \item Que valent $\EE{X^{2n+1}}$ pour $n\in\N$ ?
    \item Pour $n\in\N$, on pose $c_n=\EE{X^{2n}}$. Montrer que $c_n=(2n-1)c_{n-1}$ pour $n\in\N^*$. En déduire une formule explicite pour $\EE{X^{2n}}$.
    \item En déduire $\EE{X}$, $\VV{X}$ et $\sigma(X)$.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Loi de Cauchy)}

  Une variable aléatoire réelle $X$ est dite \emph{de Cauchy standard} si la fonction densité de $X$ est :
  \[
    \rho_X(t) = \frac{1}{\pi(1+t^2)} \text{ pour } t \in \mathbb{R}.
  \]
  \begin{enumerate}
    \item Calculez l'espérance $\mathbb{E}(X)$, si elle existe.
    \item Montrer que $\sigma X + \mu$ est une variable continue et calculer sa densité.\\
    \emph{Nous appelons cette distribution une distribution \emph{Cauchy} avec les paramètres $(\mu, \sigma)$.}
    \item Déterminer une fonction $G$ telle que pour $U\sim\Uni{[0,1]}$, la variable $G(U)$ soit de Cauchy standard.
  \end{enumerate}
\end{exo}


% -----------------------------------------------
\begin{exo}\emph{(Loi de Laplace)}

  On considère une variable aléatoire $X$ dont la densité est donnée par
  \[
    f(x)=ce^{-|x|}.
  \]
  \begin{enumerate}
    \item Calculer $c$.
    \item Démontrer que $X$ admet des moments de tout ordre. Les calculer.
  \end{enumerate}
\end{exo}

% ===============================================
\section{Variables continues - supplément}
% ===============================================



% -----------------------------------------------
\begin{exo}\emph{(Consommation d'eau)}

  La consommation journalière en eau d'une agglomération au cours du mois de juillet est une variable aléatoire $X$ dont la densité $f$ a la forme :
  \[
    \rho(t)=c(t-a)(b-t)\1_{[a,b]}(t),\quad t\in\R,
  \]
  où $a$, $b$, $c$ sont des constantes strictement positives ($a<b$).
  \begin{enumerate}
    \item Vérifier que l'on a pour tout $n\in\N$:
    \[
       \int_a^b (t-a)^n(b-t)\,dt=\frac{(b-a)^{n+2}}{(n+1)(n+2)}.
    \]
    \item Exprimer la constante $c$ en fonction de $a$ et $b$.
    \item Calculer $\EE{X-a}$ et $\EE{(X-a)^2}$. En déduire $\EE{X}$ et $\VV{X}$.
    \item Donner la fonction de répartition $F$ de la variable aléatoire $X$. Donner l'allure des représentations graphiques de $\rho$ et $F$. Proposer une interprétation physique des constantes $a$ et $b$.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Loi log-normale)}

  On dit qu'une variable positive $X$ suit une loi log-normale si $Y=\ln X$ suit une loi normale centrée réduite.
  \begin{enumerate}
    \item Exprimer la fonction de répartition de $X$ à l'aide de la fonction de répartition $\phi$ de la loi normale centrée réduite. Calculer sa densité.
    \item Démontrer que $\EE{X}=\sqrt e$.
  \end{enumerate}

\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Une loi à reconnaître)}

  Soit $\rho$ la fonction définie sur $\R$ par $\rho(x)=\frac{1}{2(1+|x|)^2}$.
  \begin{enumerate}
    \item Démontrer que $\rho$ est la densité de probabilité d'une variable aléatoire $X$.
    \item On considère la variable aléatoire $Y=\ln(1+|X|)$. Calculer sa fonction de répartition.
    \item Reconnaître la loi de $Y$.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Entropie)}

  Étant donné $X$ une variable aléatoire réelle de densité $f_X$, on appelle entropie de $X$ la quantité suivante, si elle existe,
  \[
    h(X)=-\int_{-\infty}^\infty f_X(x)\log f_X(x) dx.
  \]
  \begin{enumerate}
    \item Calculer l'entropie d'une loi aléatoire uniforme sur le segment $[a,b]$.
    \item On suppose que $X$ suit une loi normale, d'espérance $m$ et variance $\sigma^2$, i.e. $X\sim \mathcal{N}(m, \sigma^2)$, dont on rappelle la densité
    \[
      f_X(x)=\frac{1}{\sqrt{2\pi\sigma^2}}e^{-\frac{(x-m)^2}{2\sigma^2}}.
    \]
    \begin{enumerate}
      \item Rappeler l'expression de l'espérance et de la variance de $X$, sous formes d'intégrales de $f_X$.
      \item Montrer que $h(X)=\frac{1}{2}(1+\log(2\pi\sigma^2)).$
    \end{enumerate}
    \item On souhaite prouver que, parmi les variables aléatoires de variance donnée, les lois normales admettent une entropie maximale. On fixe $Y$ une variable aléatoire réelle centrée (c’est-à-dire d'espérance nulle), de densité $f_Y$ et de variance $\sigma^2$, admettant une entropie. On note $\varphi$ la densité d'une loi normale centrée ($m=0$), de variance $\sigma^2$. On suppose que les fonctions
    \[
      x\mapsto f_Y(x)\log\frac{\varphi(x)}{f_Y(x)} \quad \text{ et }\quad x\mapsto f_Y(x)\log\varphi(x)
    \]
    sont intégrables sur $\R$.
    \begin{enumerate}
      \item Démontrer que pour tout $x>0$, $\log x \leq x-1$.
      \item Vérifier que
       \[
        h(Y)=\int_{-\infty}^{+\infty}f_Y(x)\log\frac{\varphi(x)}{f_Y(x)}dx - \int_{-\infty}^{+\infty} f_Y(x)\log\varphi(x)dx.
      \]
      \item En déduire que $h(Y)\leq \frac 12(1+\log(2\pi \sigma^2)).$
    \end{enumerate}
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}

  Le nombre d'accidents en une semaine dans une usine est une v.a. de moyenne $\mu$ et de variance $\sigma^2$. Le nombre d'individus blessés dans un accident est une v.a. de moyenne $\nu$ et de variance $\tau^2$. Les nombres d'individus blessés dans des accidents différents sont indépendants.

  Donner l'espérance et la variance du nombre d'individus blessés en une semaine.
\end{exo}

\end{document}
