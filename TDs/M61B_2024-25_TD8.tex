\documentclass[a4paper,11pt,reqno]{amsart}
\usepackage{lille}
\input{m61proba}

\lilleset{titre={TD8 - Inégalités probabilistes, LGN et TCL}}

\begin{document}


% ===============================================
\section{Inégalité de Markov et de Bienaymé-Tchebychev}
% ===============================================

% -----------------------------------------------
\begin{exo}\emph{(Comparaison des inégalités de Markov et de Bienaymé-Tchebychev)}

  Le nombre de pièces sortant d’une usine en une journée est une variable aléatoire d’espérance 50. On veut estimer la probabilité que la production d'un jour donné dépasse 75 pièces.

  En utilisant l’inégalité de Markov, quelle estimation obtient-on sur cette probabilité ? Que peut-on dire de plus sur cette probabilité si on sait que la variance de la production quotidienne est $25$?
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Pièces défectueuses)}

  Une usine fabrique des pièces dont une proportion inconnue $p\in ]0,1[$ est défectueuse, et on souhaite trouver une valeur approchée de $p$. On effectue un prélèvement de $n$ pièces. On suppose que le prélèvement se fait sur un échantillon très grand, et donc qu'il peut s'apparenter à une suite de $n$ tirages indépendants avec remise. On note $S_n$ la variable aléatoire égale au nombre de pièces défectueuses et on souhaite quantifier le fait que $\frac{S_n}{n}$ approche $p$.
  \begin{enumerate}
    \item Quelle est la loi de $S_n$ ? Sa moyenne? Sa variance?
    \item Démontrer que, pour tout $\varepsilon>0$
    \[
      \PP{\abs{\frac{S_n}{n}-p}\geq \varepsilon }\leq \frac{1}{4n\varepsilon^2}.
    \]
    \item En déduire une condition sur $n$ pour que $S_n/n$ soit une valeur approchée de $p$ à $10^{-2}$ près avec une probabilité supérieure ou égale à $95\%$.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Surréservation et inégalité de Bienaymé-Tchebychev)}

  Une compagnie aérienne exploite un avion Paris-Montréal d'une capacité de 150 places. Pour ce vol, une analyse statistique a montré qu'un passager ayant réservé son billet se présentait à l'embarquement avec une probabilité de $p=0,\!75$. La compagnie souhaite optimiser le remplissage de l'avion et souhaite vendre $n$ billets, avec $n>150$, mais en limitant le risque que plus de 150 personnes se rendent à l'embarquement à moins de $5\%$. On supposera dans la suite que $np< 150$. On définit la variable aléatoire $S_n$ comme le nombre de personnes, parmi les $n$ ayant réservé un billet, se présentant à l'embarquement.

  \begin{enumerate}
    \item Quelle est la loi de $S_n$?
    \item En appliquant l'inégalité de Tchebychev à $S_{n}$, démontrer que
    \[
      P(S_n\geq 150)\leq \frac{np(1-p)}{(150-np)^2}.
    \]
    \item Résoudre sur $]0,150[$ l'inéquation $\frac{x(1-p)}{(150-x)^2}\leq 0,\!05$.
    \item Combien la compagnie peut-elle vendre de billets tout en s'assurant que la probabilité que plus de 150 clients se présentent à l'embarquement est inférieure ou égale à $5\%$?
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Loi faible des grands nombres)}

  Soit $\suite[n\geq1]{X_n}$ une suite de variables aléatoires indépendantes, de même espérance $m$ et de même variance $\sigma^2$. On pose
  \[
    \overline{X}_n=\frac{X_1+\dots+X_n}{n}.
  \]
  Démontrer, à l'aide de l'inégalité de Bienaymé-Tchebychev, que pour tout $\varepsilon>0$,
  \[
    \lim_{n\to\infty}\PP{\abs{\overline{X}_n-m}\geq \varepsilon}=0.
  \]
\end{exo}


% ===============================================
\section{Loi forte des grands nombres et théorème central limite}
% ===============================================

% -----------------------------------------------
\begin{exo}\emph{(Autour de la loi binomiale)}

  Soient $B_1, \dots ,B_n$ des v.a. de Bernoulli indépendantes de paramètre $p\in[0,1]$ et $f$ une fonction continue sur $[0,1]$.
  \begin{enumerate}
    \item Quelle est la distribution de la variable aléatoire $S_n\coloneqq B_1+\dots+B_n$ ?
    \item Exprimer $\EE{f\pa{\frac{S_n}{n}}}$ en fonction de $p$, $n$, et $f$.
    \item Après avoir justifié l'existence de la limite, calculer
    \[
      \lim_{n\to\infty}\sum_{k=0}^n \binom{n}{k} p^{k}(1-p)^{n-k}f\Bigl(\frac{k}{n}\Bigr).
    \]
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Loi forte et loi de Poisson)}

  \begin{enumerate}
    \item Soient $X$ et $Y$ deux variables aléatoires discrètes suivant chacune des lois de Poisson indépendantes de paramètres $\lambda_X$ et $\lambda_Y$.
     Soit $k\in \N$, calculer $\PP{X+Y}=k$. Que peut-on en déduire sur la distribution de la variable aléatoire $Z=X+Y$ ?
    \item Soient $X_1, \dots X_n $ des variables aléatoires de loi de Poisson, indépendantes, de même paramètre $\lambda>0$.
    \begin{enumerate}
      \item Déduire de la question précédente la distribution de la variable aléatoire $Z_n=\sum_{k=1}^n X_k$.
      \item Soit $f:\R_+\to\R$ une fonction continue et bornée. Après avoir justifié son existence, calculer
      \[
        \lim_{n\to\infty}\sum_{k=0}^{+\infty}e^{-n\lambda}\frac{(n\lambda)^k}{k!}f\Bigl(\frac{k}{n}\Bigr).
      \]
    \end{enumerate}
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(TCL et loi de Poisson)}

  Soit $\suite[i\geq 1]{X_{i}}$ une suite de variables aléatoires i.i.d. de Poisson de paramètre $\lambda=1$.
  \begin{enumerate}
    \item Soit $Z_{n} = X_{1}+\dots+X_{n}$ et $F_n$ sa fonction de répartition.
      \begin{enumerate}
        \item Pour tout $n$ entier, calculer $F_n(n)$.
        \item Calculer après, avoir justifié son existence,
        \[
          \lim_{n\to\infty}e^{-n}\sum_{k=0}^{n}\frac{n^k}{k!}.
        \]
      \end{enumerate}
      \indication{Montrer que $Z_{n}\sim \mathcal{P}(n)$.}
    \item Soit $f$ une fonction continue et bornée sur $\R$. Après avoir justifié l'existence de la limite, montrer que
      \[
        \lim_{n\to\infty }e^{-n}\sum_{k\in \N}f\pa{\frac{k-n}{\sqrt{n}}}\frac{n^k}{k!}=\frac{1}{\sqrt{2\pi}}\int_\R f(t)e^{-\frac{t^{\mathrlap{2}}}{2}}\,dt.
      \]
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Moyenne géométrique)}

  Soit $\suite[n\geq1]{U_n}$ une suite de variables aléatoires i.i.d. uniformes sur $[0,1]$, on définit $X_n=\pa{\prod_{j=1}^n U_j}^{1/n}$.
  \begin{enumerate}
    \item Que vaut $\log X_n$ ?
    \item Soit $j\in \N$ fixé, calculer pour tout $x\in \R$, $\PP{-\log U_j \leq x}$. En déduire la loi de $-\log U_j$.
    \item Montrer que $X_n$ converge p.s. quand $n\to\infty$, et déterminer sa limite.
    \item En utilisant le théorème central limite, calculer
    \[
      \lim_{n\to\infty}\PP{X_n\leq e^{-1}}.
    \]
  \end{enumerate}
\end{exo}


% ===============================================
\section{Pour approfondir}
% ===============================================

% -----------------------------------------------
\begin{exo}\emph{(Une variante de la loi faible des grands nombres)}

  Soit $(X_n)_{n\geq 1}$ une suite de variables aléatoires deux à deux indépendantes. On suppose que chaque $X_n$ suit une loi de Bernoulli de paramètre $p_n$. On note $S_n=X_1+\dots+X_n$ et on souhaite démontrer que, pour tout $\varepsilon>0$,
  $$\lim_{n\to+\infty}P\left(\left|\frac {S_n}n-\frac 1n\sum_{k=1}^n p_k\right|\geq \varepsilon\right)=0.$$

  Pourquoi ne peut-on pas appliquer directement la loi faible des grands nombres ? Quelle est l'espérance de $S_n$? Sa variance? Démontrer que $V(S_n)\leq n$. En déduire le résultat.
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Un problème chinois!)}

  On suppose qu'à la naissance, la probabilité qu'un nouveau-né soit un garçon est égale à $1/2$. On suppose que tous les couples ont des enfants jusqu'à obtenir un garçon. On souhaite évaluer la proportion de garçons dans une génération de cette population. On note $X$ le nombre d'enfants d'un couple pris au hasard dans la population.

  Donner la loi de la variable aléatoire $X$. On suppose qu'une génération en âge de procréer est constituée de $N$ couples, et on note $X_1,\cdots,X_N$ le nombre d'enfants respectif de chaque couple. On note enfin $P$ la proportion de garçons issus de cette génération. Exprimer $P$ en fonction de $X_1,\dots,X_N$. Quelle est la limite de $P$ lorsque $N$ tend vers l'infini. Qu'en pensez-vous ?
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Théorème de Bernstein-Weierstrass)}

  Soit $f$ une fonction continue de $[0,1]$ dans $\C$. Le $n$-ième polynôme de Bernstein de $f$ , $B_n$, est défini par
  \[
    B_n(x) =\sum_{k=0}^n\binom{n}{k}x^k(1-x)^{n-k}f(k/n).
  \]
  \begin{enumerate}
    \item Soit $S_n(x)$ la variable aléatoire définie par $S_n(x) = \Bin{n,x}/n$, où $\Bin{n,x} $ est une variable
    aléatoire de loi binomiale de paramètres $n$ et $x$.
    \begin{enumerate}
      \item Vérifier que $ B_n(x) = \EE{f (S_n(x}))$.
      \item Calculer $\EE{S_n(x})$ et $\VV{S_n(x)}$.
    \end{enumerate}
    \item Montrer que pour tout $\eta>0$ et pour tout $x\in [0,1]$
    \[
      \PP{|S_n(x}-x|\geq \eta)\leq \frac{1}{n\eta^2}.
    \]
    \item On rappelle que $f$ étant continue sur un segment, elle est également uniformément continue.
    \begin{enumerate}
      \item Rappeler la définition de l'uniforme continuité.
      \item Montrer que pour tout $\eta>0$
      \[
        |B_n(x)-f(x)|\leq \EE{|f(x)-f\bpa{S_n(x)}|\1_{|S_n(x)-x|< \eta}}+2\norm{f}_{\infty}\PP{|S_n(x}-x|\geq \eta).
      \]
      \item En déduire que pour tout $\varepsilon>0$, il existe $\eta>0$ tel que
      \[
        |B_n(x)-f(x)|\leq \varepsilon+2\norm{f}_{\infty}\PP{|S_n(x}-x|\geq \eta).
      \]
    \end{enumerate}
    \item Déduire des questions précédentes le Théorème de Bernstein-Weierstrass
    \[
      \sup_{x\in[0,1]}|B_n(x)-f(x)|\underset{n\to\infty}{\to} 0.
    \]
  \end{enumerate}
\end{exo}

\end{document}
