\documentclass[a4paper,11pt,reqno]{amsart}
\usepackage{lille}
\input{m61proba}

\lilleset{titre=TD1 - Evénements et tribus}

\begin{document}


% ===============================================
\section{Rappels : probabilité, indépendance, conditionnement}
% ===============================================

% -----------------------------------------------
\begin{exo}\emph{(Quizz)}

  Pour chacune des assertions suivantes, donner soit une preuve, soit un contre-exemple.
  \begin{enumerate}
    \item Si $A$ et $B$ sont deux événements indépendants et incompatibles alors l'un des deux événements au moins est de probabilité nulle.
    \item Si l'un des événements $A$ ou $B$ est de probabilité nulle alors $A$ et $B$  sont indépendants et incompatibles.
    \item Si un événement $A$ est indépendant d'un événement $B$ et d'un événement $C$, alors il est indépendant de $B\cup C$.
    \item Si un événement $A$ est indépendant d'un événement $B$ et si $C$ est un événement tel que $C\subset B$ alors $A$ est indépendant de $C$.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Une inégalité injustement méconnue)}

  Sur l'espace probabilisé $(\Omega,{\mathscr{F}},\P)$, on note $A$ un événement quelconque et $B$ un événement tel que $0<\PP{B}<1$.
  \begin{enumerate}
    \item Montrez que
    \begin{equation}\tag{$\star$}\label{170906a}
      \abs{\PP{A\cap B}-\PP{A}\PP{B}} \leq \frac{1}{4}\abs{\PP{A\mid B} - \PP{A\mid B^c}}.
    \end{equation}
  \indication{Commencez par exprimer $\PP{A\mid B} - \PP{A\mid B^c}$ en fonction des seules probabilités $\PP{A\cap B}$, $\PP{A}$, $\PP{B}$.}

  \item Que donne l'inégalité (\ref{170906a}) lorsque $A\subset B$ ?
  \item Dans quels cas (\ref{170906a}) est-elle une égalité ?
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}[.7]\emph{(Être de probabilité 1 ou ne pas être)}
  \begin{enumerate}
    \item Quelle est la probabilité d'une intersection dénombrable d'événements de probabilité 1 ?
    \item Peut-il exister $n$ événements indépendants de même probabilité $p$ dont la réunion soit l'espace $\Omega$ tout entier ?
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Tirer un nombre réel au hasard)}

  On décide de \enquote{tirer au hasard} un réel $x \in [0,1[$.  Pour cela on
  effectue --- par la pensée! --- une infinité de tirages avec remise dans une
  urne contenant des boules marquées chacune d'un chiffre décimal. On construit
  $x$ en écrivant --- toujours par la pensée --- son développement décimal
  illimité, le numéro sorti au $i$\ieme\ tirage fournissant le $i$\ieme\ chiffre
  décimal de $x$. La composition précise de l'urne est inconnue. On sait
  seulement qu'il y a au moins une boule marquée $0$, qu'il n'y a aucune boule
  marquée $9$ et qu'il y a au moins une boule marquée d'un autre chiffre que
  $0$.  Comme aucune boule dans l'urne n'est marquée $9$, le développement
  décimal illimité ainsi obtenu est forcément propre. On note $p$ la probabilité
  de sortir une boule marquée $0$ au $i$\ieme\ tirage. En raison du mode de
  tirage (une boule avec remise) et de la composition de l'urne, il est clair
  que $p$ ne dépend pas de $i$ et que $0<p<1$.

  Pour chaque $ i $ de $ \N^* $ on note $ N_i $ l'événement :
  \[
    N_i \coloneqq \{ \text{le $i$\ieme\ chiffre  de $x$ après la virgule est un zéro}\}
  \]
  Les $N_i$ sont indépendants et tous de probabilité $p$.

  \begin{enumerate}
    \item Exprimer par des opérations ensemblistes sur les $N_i$ les événements :
    \begin{align*}
      D_n \coloneqq&\, \{\textrm{l'écriture décimale illimitée de $ x $ ne comporte que des zéros à partir du rang $ n $}\},\\
      D \coloneqq  &\, \{\textrm{$ x $ est un nombre décimal}\},\\
      E \coloneqq  &\, \{\textrm{l'écriture de $ x $ comporte une infinité de zéros}\}.
    \end{align*}
    Comparer $D$ et $E$.

    \item Donner $ \PP{ \{x < 10^{-4}\} } $. Les $ N_i $, $ i \in \N^* $, sont-ils deux à deux disjoints ?

    \item Calculer la probabilité de l'évènement
    \[
      F_n \coloneqq \{\textrm{le premier chiffre non-nul après la virgule est au rang $ n $}\}.
    \]
    Les $ F_n $, $ n \in \N^* $, sont-ils deux à deux disjoints ?

    \item Écrire $\bigcup_{i=1}^{+\infty} N_i^c $ à l'aide des $ F_n $ et calculer sa probabilité. En déduire $ \PP{\{x=0\}} $.

    \item Pour $m \in \N^*$, calculer la valeur de $ \PP{D_m} $. On pourra utiliser la monotonie de la suite $\suite[n \ge m]{G_n}$ où $G_n \coloneqq \bigcap_{i=m}^n N_i $.

    \item Quelle est la probabilité d'obtenir un nombre décimal avec ce type de tirage aléatoire ?

    \item Reprendre la question précédente avec une urne dans laquelle on a rajouté une boule numerotée~$9$.
  \end{enumerate}
\end{exo}


\newpage
% ===============================================
\section{Intégrabilité, limsup, liminf}
% ===============================================

% -----------------------------------------------
\begin{exo}\emph{(Fonction non Riemann intégrable)}
  \begin{enumerate}
    \item Soit $h$ la fonction définie sur $[0,1]$ par
    \[
      h(x)=
        \begin{cases}
          1 & \text{si } x\in ]0,1],\\
          0 & \text{si } x=0.
        \end{cases}
    \]
    Vérifier que $h$ est Riemann intégrable.
    \item Considérons $f(x)=\1_{[0,1]\cap \Q}(x)$. Montrer que $f$ n'est pas Riemann intégrable sur $[0,1]$.
    \item Soit $g$ la fonction définie sur $[0,1]$ par
    \[
      g(x) =
          \begin{cases}
            0           & \text{si }x\in [0,1]\setminus \Q,                         \\
            \frac{1}{q} & \! \text{si }x=\frac{p}{q}, \text{ avec pgcd}(p,q)=1, 0<p\leq q, \\
            1           & \text{si }x=0.
          \end{cases}
    \]
    Montrer que $g$ est Riemann intégrable et que $f=h\circ g$. Que peut-on en déduire ?

    \item On rappelle que $[0,1]\cap \Q$ est dénombrable et on note $(r_n)_{n\in \N^*}$ une numérotation de cet ensemble. On pose pour tout $n\in \N^*$, $f_n(x)=\1_{\{r_1,\dots, r_n\}}(x)$.

    Montrer que $f_n$ est Riemann intégrable sur $[0,1]$.
    \item Montrer que $(f_n)_n$ converge simplement vers $f$. Que peut-on en déduire ?
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Limites supérieures et inférieures d'ensemble)}

  Soit $\suiteN{A_n}$ une suite de parties d'un ensemble $\Omega$, on définit
  \[
    \limsup A_n=\bigcap_{n\geq 0}\bigcup_{p\geq n}A_p \quad \text{ et } \quad \liminf A_n=\bigcup_{n\geq 0}\bigcap_{p\geq n}A_p.
  \]
  \begin{enumerate}
    \item Justifier que $\limsup_{n\to +\infty} A_n$ est l'ensemble des éléments de $\Omega$ appartenant à une infinité de $A_n$ et $\liminf_{n\to +\infty} A_n$ est l'ensemble des éléments de $\Omega$ appartenant  à  tous les $A_n$ sauf à un nombre fini.

    \item Montrer que si $(A_n)_{n\in\N}$ est croissante pour l'inclusion, alors
    \[
      \limsup_{n\to +\infty} A_n=\liminf_{n\to +\infty} A_n=\bigcup_{n\in\N}A_n.
    \]

    \item Montrer que si $(A_n)_{n\in\N}$ est décroissante pour l'inclusion, alors
    \[
      \limsup_{n\to +\infty} A_n=\liminf_{n\to +\infty} A_n=\bigcap_{n\in\N}A_n.
    \]

    \item Montrer que $\1_{\cup_{p\geq n}A_p} = \sup_{p\geq n} \1_{A_p}$ et $\1_{\cap_{p\geq n} A_p} = \inf_{p\geq n} \1_{A_p}$. En déduire les que $\1_{\limsup A_n} = \limsup \1_{A_n}$ et $\1_{\liminf A_n} = \liminf \1_{A_n}$.

    \item Montrer que
    \begin{enumerate}
      \item $(\limsup A_n)^c=\liminf A_n^c$.
      \item $\liminf A_n \subset \limsup A_n$.
      \item $\limsup A_n=\ensemble{x\in\Omega}{\sum_{n\in \N} \1_{A_n}(x)=+\infty}$.
      \item $\liminf A_n=\ensemble{x\in\Omega}{\sum_{n\in \N} \1_{A_n^c}(x)<+\infty}$.
      \item $\limsup(A_n\cup B_n)=\limsup A_n \cup \limsup B_n$.
      \item $\limsup (A_n \cap B_n)\subset \limsup A_n \cap \limsup B_n$.
    \end{enumerate}

    \item Calculer $\limsup A_n$ et $\liminf A_n$ dans les cas suivants :
    \begin{enumerate}
      \item $A_n=]-\infty, a_n]$ avec $a_{2p}=1+\frac{1}{2p}$ et $a_{2p+1}=-1-\frac{1}{2p+1}$.
      \item $A_{2p}=]0,3+\frac{1}{3p}[$ et $A_{2p+1}=]-1-\frac{1}{3p},2]$.
      \item $A_k=p_k \N$ où $\suite[k\in \N]{p_k}$ est la suite des nombres premiers.
    \end{enumerate}
  \end{enumerate}
\end{exo}


% ===============================================
\section{Tribus}
% ===============================================

% -----------------------------------------------
\begin{exo}

  On rappelle que la tribu des boréliens de $\R^2$ est engendrée par les ouverts de $\R^2$. Démontrer que les ensembles suivants sont des boréliens de $\R^2$ :
  \begin{enumerate}
    \item $\Delta=\{(x,x)\in\R^2\ / \ x\in\R\}$.
    \item $B=\{(x,y)\in \R^2\ / \ x^2 +y^2 = 1 \text{ et } x\notin
    \Q \}$.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Quizz)}

  Les affirmations suivantes sont-elles vraies ou fausses? Justifier brièvement ou donner un contre-exemple.
  \begin{enumerate}
    \item Soient $\mathscr{T}$ et $\mathscr{T}'$ deux tribus sur $\Omega$. Alors $\mathscr{T}\cup\mathscr{T}'$ est une tribu sur $\Omega$.
    \item Si $A$ est un ensemble inclus dans un ensemble $B$ mesurable, alors $A$ est mesurable.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Union et intersection de tribus)}

  \begin{enumerate}
    \item Soit $\Omega=\{1,2,3,4\}$.
    \begin{enumerate}
      \item Identifier la tribu $\mathcal F_1$, engendrée par le singleton $\{1\}$ et la tribu $\mathcal F_2$ engendrée par $\{2\}$.
      \item Identifier $\mathcal F_1\cap \mathcal F_2$ et $\mathcal F_1\cup \mathcal F_2$. Est-ce que ce sont des tribus?
    \end{enumerate}
    \item Soit $\Omega$ un ensemble quelconque et $\mathcal C_1$ et $\mathcal C_2$ des collections d'ensembles de $\Omega$. Donc $\mathcal C_i\subset \mathcal P(\Omega)$.
    \begin{enumerate}
      \item Montrer que $\mathcal F_{\mathcal C_1\cap \mathcal C_2}\subset \mathcal F_{\mathcal C_1}\cap \mathcal F_{\mathcal C_2}$.
      \item Montrer qu'il n'est pas toujours vrai que $ \mathcal F_{\mathcal C_1}\cap \mathcal F_{\mathcal C_2}\subset \mathcal F_{\mathcal C_1\cap \mathcal C_2}$.
    \end{enumerate}
    \item Soit $\mathcal C\subset \mathcal P(\Omega)$.
    \begin{enumerate}
      \item Soit $\mathcal C^\mathrm{c}=\mathcal P(\Omega)\setminus \mathcal C$. Est-il vrai que $\mathcal F_{\mathcal C}\cap \mathcal F_{\mathcal C^\mathrm{c}}=\emptyset$? Est-il vrai que $\mathcal F_\mathcal C=\mathcal F_{\mathcal C^{\mathrm c}}$?
      \item Soit $\mathcal C\subset \mathcal P(\Omega)$ et soit $\mathcal D=\{A\in \mathcal P(\Omega)| A^{\mathrm{c}}\in \mathcal C\}$. Est-il vrai que
          $\mathcal F_\mathcal C=\mathcal F_\mathcal D$?
    \end{enumerate}
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Tribu, expérience, information)}

  \begin{enumerate}
    \item On lance un dé à quatre faces. Ecrire l'ensemble $\Omega$ de tous les résultats possibles.
    \item Une personne lance le dé et nous annonce le résultat tiré. Ecrire la tribu $\mathcal{F}_2$ de tous les évènements observables pour nous.
    \item La personne lance le dé et nous annonce seulement \enquote{pair} ou \enquote{impair}. Ecrire la tribu $\mathcal{F}_1$ des évènements observables pour nous.
    \item La personne lance le dé et n'annonce rien ! A quelle la tribu $\mathcal{F}_0$ correspond cette fois l'expérience que nous observons ?
    \item On dit que $(\mathcal{F}_0, \mathcal{F}_1, \mathcal{F}_2)$ est une suite croissante de tribus. En quoi est-elle croissante ? Qu'est-ce qui augmente, intuitivement, le long de cette suite ? Dans l'espace probabilisé $(\Omega,\mathcal{F},P)$, que représente la tribu $\mathcal{F}$ ?
    \item La modélisation mathématique des produits financiers dérivés (stock-options) utilise des espaces probabilisés complexes. Ils sont munis de tribus $\mathcal{F}_t$ indexées par la date et contenant tous les événements observables de l'origine jusqu'à la date $t$. L'affirmation $\mathcal{F}_{\text{24 février 2022}} \subset \mathcal{F}_{\text{1 avril 2024}}$ est considérée par les quants comme une évidence. Pourquoi ? Quelle capacité (réaliste ?) présuppose ce choix de modélisation ?
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Une tribu sur les entiers)}

  Soit $\Omega=\Z$. On considère $\mathcal{T}$ la tribu engendrée
  par les ensembles
  \[
    S_n=\{n,n+1,n+2\},\quad\text{avec } n\in\Z.
  \]
  \begin{enumerate}
    \item Montrer que quel que soit $n\in\Z$, $\{n\}$ appartient à
    $\mathcal{T}$.
    \item En déduire $\mathcal{T}$.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Tribu engendrée par une partition)}\label{exo:partition}

  Soit $E$ un ensemble et $\{E_1,E_2,\dots,E_n\}$ une partition de $E$, c'est-à-dire
  \[
    E=\bigcup_{i=1}^n E_i,\qquad E_i\cap E_j=\emptyset, \, i\neq j.
  \]
  Montrer que
  \[
    \sigma(\{E_1,E_2,\dots,E_n\})=\left\{\bigcup_{i\in I}E_i:\,\,I\subset\{1,2,\dots,n\}\right\}.
  \]
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Ensemble engendrant la tribu borélienne)}

  Soit $\mathcal{B}(]0,1[)$ la tribu Borélienne sur $]0,1[$.

  \begin{enumerate}
    \item Montrer que tout ouvert de $]0,1[$ peut s'écrire comme réunion dénombrable d'intervalles de $]0,1[$ de la forme $[r-\delta, r+\delta]$ où $r$ et $\delta$ sont des rationnels de $]0,1[$.

    \item  Montrer que $\mathcal{B}(]0,1[)$ est engendrée par chacune des familles suivantes :
    \begin{enumerate}
      \item  $\mathcal{C}_1=\{[a,b],\ a\leq b, \ a,b\in ]0,1[\}$.
      \item  $\mathcal{C}_2=\{[a,b],\ a\leq b, \ a,b\in ]0,1[\cap \Q\}$.
      \item  $\mathcal{C}_3=\{]0,t],\ t\in  ]0,1[\}$.
      \item  $\mathcal{C}_4=\big\{\big]0,\frac{1}{2^n}\big[, \ \big[\frac{k}{2^n},\frac{k+1}{2^n}\big[,\ n\in \N, \ k\in \{1,\dots 2^n-1\} \big\}$.
    \end{enumerate}
    \item Considérons pour tout $n\in \N$ la famille suivante d'ensembles de $]0,1[$ :
    \[
      \mathscr{T}_n=\sigma\Big(\big]0,1/2^n\big[, \ \big[k/2^n,(k+1)/2^n\big[, \ k\in \{1,\dots 2^n-1\} \Big).
    \]
    Montrer que la suite des $\suiteN{\mathscr{T}_n}$ est croissante au sens de l'inclusion mais que $\bigcup_{n\in \N} \mathscr{T}_n$ n'est pas une tribu.\\
    \indication{%
      On pourra vérifier que
      \[
        \{1/2\}=\bigcap_{n\geq 1}\left[\frac{k_n}{2^n},\frac{k_n+1}{2^n} \right[,
      \]
      pour une certaine suite d'entiers $k_n\in\{1,\dots,2^n-1\}$ et raisonner par l'absurde en montrant que si $\bigcup_{n\in \N} \mathscr{T}_n$ est une tribu, alors $\{1/2\}\in\mathscr{T}_n$, pour un certain $n$. On conclura à une absurdité en utilisant l'exercice \ref{exo:partition}.
    }
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Tribu engendrée par les singletons)}
  Soit $\Omega$ un ensemble non vide et $\mathscr{T}=\{A\in\mathcal P(\Omega):A \text{ dénombrable ou } \Omega\setminus A \text{ dénombrable}\}$.
  \begin{enumerate}
    \item Vérifier que $\mathscr{T}$ est une tribu sur $\Omega$.
    \item Montrer que la tribu engendrée par les singletons de $\Omega$ est $\mathscr{T}$.
    \item Quelle est la tribu engendrée par l'ensemble des parties finies de $\Omega$?
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Points de convergence d'une suite d'applications)}

  Soit $\suite[n\geq1]{f_n}$ une suite d'applications mesurables de $\R$ dans lui-même.
  \begin{enumerate}
    \item Soit $x\in\R$. Montrer que $\suite[n\geq1]{f_n(x)}$ converge vers $l$ si et seulement si pour tout $n\in\N^*$, il existe $k\in\N$ tel que pour tout $l\geq k$, $|f_l(x)-l|<\frac1n$.
    \item Soit $A\coloneqq\{x\in\R:f_n(x)\underset{n\to +\infty}{\longrightarrow}0\}$. Montrer que
    \[
      A=\bigcap_{n\in\N^*}\bigcup_{k\in\N} \bigcap_{l\geq k} f^{-1}_l\left]-1/n,1/n\right[.
    \]
    En déduire que  $A$  est un borélien de $\R$.
    \item Montrer que
    $B\coloneqq \{x\in\R:\suite[n\geq1]{f_n(x)}\text{ admet $0$ comme valeur d'adhérence}\}$ est un borélien de $\R$.
    \item Montrer que $C\coloneqq \{x\in\R:\suite[n\geq1]{f_n(x)} \text{ admet une limite}\}$
    est un borélien de $\R$.\\
    \indication{Utiliser le critère de Cauchy pour caractériser l'existence d'une limite.}
  \end{enumerate}
\end{exo}

\end{document}
