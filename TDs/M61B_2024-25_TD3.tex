\documentclass[a4paper,11pt,reqno]{amsart}
\usepackage{lille}
\input{m61proba}

\lilleset{titre=TD3 - Mesures et mesure de Lebesgue}

\begin{document}


% ===============================================
\section{Exemples et propriétés des mesures}
% ===============================================

% -----------------------------------------------
\begin{exo}\emph{(La mesure de comptage)}

  Soit $X$ un ensemble non vide. Pour $A\in \mathcal{P}(X)$, on pose
  \[
    c(A)\coloneqq\text{card}(A).
  \]
  Montrer que $c$ est une mesure sur $(X,\mathcal{P}(X))$. \emph{Cette mesure s'appelle la mesure de comptage sur $X$.}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Tribus des évènements triviaux)}

  Soit $(\Omega,\mathscr{F},\P)$ un espace de probabilité. On note
  \[
    \mathscr{T}=\{ A\in \mathscr{F} \mid \PP{A}=0 \text{ ou } \PP{A}=1\}.
  \]
  Montrer que $\mathscr{T}$ est une tribu.
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Mesures invariantes)}

  Soit $\mu$ une mesure sur $(\Z,\mathcal{P}(\Z))$. On suppose que $\mu$ est non nulle et invariante par translation, c.-à-d. pour tout $A\in\mathcal{P}(\Z)$ et tout $n\in\Z$, $\mu(n+A)=\mu(A)$ où $n+A=\ensemble{n+p}{p\in A}$.
  \begin{enumerate}
    \item Montrer qu'il existe $n_0\in\Z$ tel que $\mu(\{n_0\})\neq 0$.
    \item Montrer que pour tout $n\in\Z$, $\mu(\{n_0\})=\mu(\{n\})$.
    \item En déduire que $\mu(\Z)=+\infty$.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Ensembles de mesure positive)}

  Soit $f:(E,\mathscr{T})\longrightarrow (\R,\mathcal{B}(\R))$ une fonction mesurable. Soit $\mu$ une mesure sur $(E,\mathscr{T})$.
  \begin{enumerate}
    \item Montrer que si $\mu(E)\neq 0$, alors il existe $A\in\mathscr{T}$, $\mu(A)\neq 0$ tel que $f$ soit bornée sur $A$.
    \item Justifier que $\{f\neq 0\}\in\mathscr{T}$.
    \item Montrer que si $\mu(\{f\neq 0\})\neq 0$, alors il existe $A\in\mathscr{T}$, $\mu(A)\neq 0$ tel que $|f|$ est minorée sur $A$ par une constante strictement positive.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Continuité de la mesure)}

  Soit $(E,\mathscr{T},\mu)$ un espace mesuré et $f:(E,\mathscr{T})\to (\R,\mathcal{B}(\R))$ une fonction mesurable. On suppose que $\mu(\{x\in E\mid f(x)>0\})>0$. Démontrer qu'il existe $\varepsilon>0$ tel que
  \[
    \mu(\{x\in E \mid f(x)>\varepsilon\})>0.
  \]
\end{exo}


% ===============================================
\section{Autour de la mesure de Lebesgue}
% ===============================================

% -----------------------------------------------
\begin{exo}\emph{(Relations entre mesure, topologie et métrique)}

  On note $\lambda$ la mesure de Lebesgue sur $\R$.
  \begin{enumerate}
    \item Soit $U$ un ouvert borné de $\R$. Montrer que $\lambda(U)<+\infty$. La réciproque est-elle vraie ? (Considérez des ouverts centrés en $n$ pour $n\in\N$.)
    \item Soit $\varepsilon>0$. Construire un ouvert $U$ dense dans $\R$ de sorte que $\lambda(U)\leq\varepsilon$. Pour cela, on pourra considérer une partie dense et dénombrable de $\R$.
    \item Soit $A$ un borélien de $\R$. Montrer que si $A$ contient un ouvert non vide, alors $\mu(A)>0$. Si $B=[0,1]\setminus\Q$, que vaut $\lambda(B)$ ? $B$ peut-il contenir un ouvert ?
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}[.49]\emph{(Lebesgue nulle et intérieur)}

  Montrer que tout sous-ensemble mesurable $E\subset \R^d$ de mesure de Lebesgue nulle est d'intérieur vide.
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Un calcul avec la mesure de Lebesgue)}

  On considère $\R$ muni de la tribu borélienne $\mathcal{B}(\R)$ et de la mesure de Lebesgue $\lambda$. Pour $n\geq 0$, on pose $A_n=]n,n+2^{-n}[$.
  \begin{enumerate}
    \item Justifier que pour tout $n\geq 0$, on a $A_n\in\mathcal{B}(\R)$.
    \item Soit $A=\bigcup_{n\geq 0}A_n$. Justifier que $A\in \mathcal{B}(\R)$. Calculer $\lambda(A)$.
    \item Un borélien de $\R$ de mesure (de Lebesgue) finie est-il nécessairement borné?
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Un deuxième calcul avec la mesure de Lebesgue)}

  Soit $\suite[n\geq 1]{a_n}$ une suite de nombre réels. On pose
  \[
    A=\ensemble{x\in\R}{ \exists n\in\N^*, |x-a_n|\leq 2^{-n}}
  \]
  \begin{enumerate}
    \item Justifier que $A\in\mathcal{B}(\R)$.
    \item Montrer que $1\leq \lambda(A)\leq 2$ (ici comme d'ha\-bi\-tu\-de $\lambda$ désigne la mesure de Lebesgue sur $\R$).
    \item Calculer $\lambda(A^c)$ où $A^c$ désigne le complémentaire de $A$ dans $\R$.
    \item Calculer $\lambda(A)$ lorsque $a_n=0$ pour tout $n\geq 1$.
    \item Calculer $\lambda(A)$ lorsque $a_n=n$ pour tout $n\geq 1$.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Continuité et notion de presque partout)}

  Soit $\lambda$ la mesure de Lebesgue sur $[0,1]$ (c.-à-d. la restriction à $[0,1]$ de la mesure de Lebesgue). Soit $f:[0,1]\longrightarrow\R$ une fonction continue et supposons que $f$ est nulle sur un borélien de mesure $1$, c.-à-d. que $\lambda\pa{f^{-1}(0)}=1$. Montrer alors que $f$ est identiquement nulle. Le résultat subsiste-t-il si on remplace continue par mesurable?
\end{exo}

\end{document}
