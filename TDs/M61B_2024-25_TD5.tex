\documentclass[a4paper,11pt,reqno]{amsart}
\usepackage{lille}
\input{m61proba}

\lilleset{titre=TD5 - Variables à densité}

\begin{document}

\emph{Dans toute la fiche, on supposera que les variables aléatoires d’un même exercice sont définies sur un même espace de probabilités $(\Omega,\mathscr F, \P)$.}

% ===============================================
\section{Autour de la fonction de répartition}
% ===============================================

% -----------------------------------------------
\begin{exo}\emph{(Fonction de répartition?)}

  Les fonctions suivantes sont-elles des fonctions de répartition d'une variable aléatoire réelle ?
  \[
    F(x) = \sin (x), \quad G(x) = \frac{1}{\pi}\big(\arctan (x) + \frac{\pi}{2}\big), \quad H(x) = \frac{1}{4} \1_{[-1,0[}(x) +  \frac{3}{4} \1_{[0,1]}(x) + \1_{]1,+\infty[}(x).
  \]
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Répartition uniforme)}

  Soit $X$ une variable aléatoire réelle de loi uniforme sur $[0,1]$.
  \begin{enumerate}
    \item Quelle est la fonction de répartition de $X$ ?
    \item Déterminer la loi de la variable aléatoire $Y$ dans les cas suivants :
    \begin{enumerate}
      \item $Y=1-X$ ;
      \item $Z=a+(b-a)X$, où $a$ et $b$ sont deux réels tels que $a<b$.
    \end{enumerate}
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Le $\min$ est-il continu ?)}

  Soit $X$ une variable aléatoire réelle de fonction de répartition $F_X$. On pose $Z=\min (X,c)$ où $c$ est un réel.
  \begin{enumerate}
    \item Calculer la fonction de répartition de $Z$.
    \item Si la loi de $X$ a pour densité $f$, est-ce que la loi de $Z$ est encore à densité ?
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Somme de v.a. uniformes)}

  Soit $X$ une variable aléatoire de loi uniforme sur $[0,1]$ et $Y$ la variable aléatoire définie par
  \[
    Y(\omega) \coloneqq
      \begin{cases}
        X(\omega)   & \text{si } X(\omega) \in [0,1/4] \cup [3/4, 1];\\
        1-X(\omega) & \text{si } X(\omega) \in\ ]1/4, 3/4[.
      \end{cases}
  \]
  \begin{enumerate}
    \item Quelle est la loi de $Y$ ?
    \item Trouver la fonction de répartition de la variable aléatoire $Z \coloneqq X+Y$ et vérifier que $Z$ n'est ni discrète ni à densité.
  \end{enumerate}
\end{exo}


% ===============================================
\section{Variables aléatoires à densité}
% ===============================================

% -----------------------------------------------
\begin{exo}\emph{(Interprétation du graphique d'une densité)}

  La variable aléatoire $X$ a pour densité la fonction $f$ ci-dessous :
  \begin{enumerate}
    \item En exploitant les informations du graphique, donner les valeurs des probabilités suivantes:\vspace{-17pt}
      \imageR[width=70mm, y offset=-11pt]{%
        img_densite.pdf
      }{%
        \[
          \begin{array}{lll}
            \PP{X\leq -2},\quad & \PP{X=-1},\quad & \PP{X\in[-2;0]},\\
            \PP{X>1},           & \PP{X \ge 1},              & \PP{|X| > 1}.
          \end{array}
        \]
      }\vspace{4pt}
    \item Déterminer la fonction de répartition de $X$.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Fonction de répartition à partir de densité)}

  Soit $X$ une v.a.r. de fonction de répartition $F$ donnée par
  \vspace{-11pt}
  \[
    \qquad
    \forall u \in \R, \quad
    F(u) = \int_{-\infty}^u f(t) d  t,
      \quad \text{avec} \quad
    f(t)=
      \begin{cases}
          1+t    & \text{si } t\in[-1,0],\\
          \alpha & \text{si } t\in]0,2],\\
          0      & \text{sinon.}
      \end{cases}
  \]
  \vspace{-21pt}
  \begin{enumerate}
    \item Représenter $f$.
    \item Calculer $F$, et en déduire $\alpha$.
    \item Représenter $F$.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Tige brisée, DS1 2024)}

  Dans tout l'exercice, $X$ désigne une variable aléatoire à valeurs dans $]0,1[$ et de loi uniforme sur $]0,1[$. On pose
  \[
    Z\coloneqq\frac{1-X}{X}.
  \]
  \begin{enumerate}
    \item Calculer explicitement la fonction de répartition de la variable aléatoire positive $Z$.\\ Dessiner son graphe.
    \item La loi de $Z$ est-elle à densité ? Si oui, la calculer et dessiner son graphe.
    \item Expliquer, si possible sans calcul, pourquoi $Z$ et $1/Z$ ont \emph{même loi}.
    \item On brise une tige de longueur $1$ en choisissant au hasard le point de rupture suivant une loi uniforme sur $]0,1[$. Quelle est la probabilité que l'un des deux morceaux soit plus de deux fois plus long que l'autre ?
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Loi de Rayleigh)}

  Soit $U$ une variable aléatoire réelle suivant la loi uniforme sur l'intervalle $]0,1]$.
  \begin{enumerate}
    \item Rappeler la fonction de répartition $F_U$ de $U$.
  \end{enumerate}
  Soit $\sigma >0$. On définit une nouvelle variable aléatoire réelle, $X$, par
  \[
    X= \sigma \sqrt{-2 \log U}.
  \]
  \begin{enumerate}[resume]
    \item Calculer et représenter graphiquement la fonction de répartition $F_X$ de $X$.
    \item La variable aléatoire $X$ est-elle à densité ? Si oui, donner une densité de $X$ et tracer son graphe.
    \item Retrouver sans calcul la valeur de l'intégrale $\displaystyle\int_0^\infty te^{-\frac{t^2}{2\sigma^2}} dt$.
  \end{enumerate}
\end{exo}


% ===============================================
\section{Loi exponentielle}
% ===============================================

% -----------------------------------------------
\begin{exo}\emph{(Simulation)}

  Soit $X\sim\Exp{a}$, une variable aléatoire suivant une loi exponentielle de paramètre $a >0$, et dont on note $F$ la fonction de répartition.
  \begin{enumerate}
    \item Sur quel intervalle maximal $F$ est-elle bijective ? Déterminer sa réciproque, $G$, sur cet intervalle.
    \item Soit $U$ une variable aléatoire suivant une loi uniforme sur $[0,1[$. On pose $Y \coloneqq G(U)$. Quelle est la loi de $Y$ ?
    \item Pour conclure, expliquer pourquoi il suffit et il est intéressant de considérer $-\frac{\log U}{a}$ pour simuler une variable aléatoire de loi exponentielle de paramètre $a > 0$.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Désintégration radioactive)}
  \newcommand*{\eexp}[1]{\operatorname{e}^{#1}}

  On dit qu'une variable aléatoire $T$ à valeurs dans $\mathbb R_+$ est \emph{sans mémoire} si elle vérifie, pour tous $s,t> 0$.
  \[
    \PP{T>t+s}=\PP{T>t}\PP{T>s}.
  \]
  \begin{enumerate}
    \item Montrer que si $T$ est une v.a. sans mémoire alors $\PP{T> t+s \mid T> t}=\PP{T>s}$, pour tous $s,t> 0$.
    \item Soit $T\sim\Exp{a}$ est une variable aléatoire de loi exponentielle de paramètre $a$, donc de densité $g(t)=a\eexp{-at}\1_{]0, +\infty[}(t)$. Montrer que $T$ est sans mémoire.
    \item La durée de vie des atomes de radon suit une loi exponentielle. La probabilité qu'un atome de radon ne soit pas désintégré en 40s sachant qu'il ne l'est pas en 12s vaut $\frac{\sqrt 2}2$. Quelle est la probabilité qu'il ne soit pas désintégré avant 76s sachant qu'il ne l'est pas en 20s?
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Minimum de deux lois exponentielles)}

  \begin{enumerate}
    \item Soient $X_1$ et $X_2$ deux variables aléatoires indépendantes suivant une loi exponentielle de paramètres respectifs $\lambda_1$ et $\lambda_2$. On pose $Y=\min(X_1,X_2)$.

    Pour tout réel $y$, calculer $\PP{Y>y}$. En déduire que $Y$ suit une loi exponentielle de paramètre $\lambda_1+\lambda_2$.
    \item \emph{(application)} Deux guichets sont ouverts à une banque. Le temps de service au premier guichet (resp. au deuxième) suit une loi exponentielle de moyenne 20 min (resp. 30 min). Deux clients rentrent simultanément, l'un choisit le guichet 1 et l'autre le guichet 2. En moyenne, après combien de temps sort le premier? En moyenne, après combien de temps sort le deuxième ?
  \end{enumerate}
\end{exo}

% ===============================================
\section{Loi normale}
% ===============================================

\emph{%
Dans cette section on suppose connues les valeurs de la fonction de répartition $\Phi$ de la loi normale centrée réduite $\mathcal{N}(0;1)$:
\[
   \Phi(x)=\frac1{\sqrt{2\pi}}\int_{-\infty}^x\operatorname e^{-\frac12t^2}\,\mathrm dt.
\]
Autrefois on lisait ces valeurs dans un tableau, de nos jours on utilise une « calculatrice » pour les obtenir.
}

% -----------------------------------------------
\begin{exo}\emph{(Notes des etudiants)}

  On suppose que les notes d'un contrôle de probabilité suivent une loi normale de paramètre $(8,5;4)$.
  \begin{enumerate}
    \item Quelle est la probabilité pour un étudiant d'avoir la moyenne ?
    \item On veut améliorer les notes à l'aide d'une transformation affine $Y=aX+b$. Déterminer $a$ et $b$ pour qu'un étudiant ait la moyenne avec une probabilité de $1/2$ et une note supérieure à $8$ avec une probabilité de $3/4$.
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo}\emph{(Temps de transport)}

  Les trajets dont il est question dans cet exercice sont censés suivre des lois normales et être indépendants.
  \begin{enumerate}
    \item Un employé E quitte son domicile à 8h30. La durée moyenne de son trajet à son lieu de travail est 25 minutes et son écart-type 10 minutes. Quelle est la probabilité qu'il arrive avant 9h sur son lieu de travail ?
    \item Un autre employé, F, doit utiliser consécutivement deux moyens de transport pour se rendre à son travail. Il prend le train à 8h20, et son bus démarre à 8h45. La durée moyenne de son trajet en train est 23 minutes et, d'autre part, la probabilité que ce trajet dure entre 18 et 28 minutes est 0,6915. La durée moyenne de son trajet en bus est 14 minutes et son écart-type est 2 minutes. Quel est l'écart-type du trajet en train ? Quelle est la probabilité que $F$ arrive avant 9h sur son lieu de travail ?
    \item Seuls E et F ont une clé de leur lieu de travail. Quelle est la probabilité que cette agence ouvre avant 9h ?
    \item Même question dans l'hypothèse où ils doivent être présents tous deux pour l'ouverture.
    \item On rappelle qu'en l'état actuel des choses, le voyage dans le temps n'existe pas. Est-il raisonnable de modéliser un temps de trajet par une loi normale ?
  \end{enumerate}
\end{exo}

% -----------------------------------
\begin{exo}\emph{(Premiers mots et normalité)}

  Un chercheur s'intéresse à l'âge moyen auquel les premiers mots du vocabulaire apparaissent chez les jeunes enfants. Il effectue une étude auprès d’un millier de jeunes enfants. Cette étude montre que les premiers mots apparaissent, en moyenne, à 11,5 mois, avec un écart-type de 3,2 mois. Le chercheur décide alors de modéliser cet âge par une variable aléatoire réelle $X$ de loi normale $\mathcal{N}(11,5 ; 3,2)$.
  \begin{enumerate}
    \item Calculer la probabilité qu'un enfant acquière ses premiers mots avant l'âge de 10 mois ?
    \item Calculer la probabilité qu'un enfant acquière ses premiers mots après l'âge de 18 mois ?
    \item Afin de détecter d'éventuels problèmes de langage chez un jeune enfant, on décide de définir un seuil d'alerte $s$ (en mois), à partir duquel on considérera qu'il n'est pas \enquote{normal} qu'un enfant n'ait pas acquis ses premiers mots. Le chercheur propose donc de choisir $s$ tel que la probabilité pour qu'un enfant ait acquis ses premiers mots avant $s$ mois soit de $99,8 \%$. Déterminer $s$.
  \end{enumerate}
\end{exo}


% -----------------------------------------------
\begin{exo}[.7]\emph{(Loi du $\chi^{2}$)}

  Soit X une variable aléatoire de loi normale $\mathcal{N}(0;1)$ et $Z=X^{2}$. Calculer la fonction de répartition et la densité de $Z$.\\
  \emph{Remarque : la loi de Z est appelée loi du $\chi^{2}$ à 1 degré de liberté.}
\end{exo}



\end{document}
